import argparse
from bs4 import BeautifulSoup
import datetime
import json5
import asyncio
import aiohttp
import re
from collections import defaultdict

HEADERS = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36"}
soup = lambda html: BeautifulSoup(html, 'html.parser')

# paging selectors
page_count = lambda p: p.select_one('[data-testid="pagination"] li:last-of-type button').get_text()
hotel_links = lambda p: [t['href'] for t in p.select('[data-testid="title-link"]')]

def room_pricing(room):
    pax_prices = [[int(r['b_max_persons']), float(r['b_raw_price'])] for r in room['b_blocks']]
    rate_by_pax = defaultdict(list) 
    for pax, rate in pax_prices:
        rate_by_pax[pax].append(rate)
    return [f"{pax}-{min(rates)}" for pax, rates in rate_by_pax.items()]

def room_pics(room, all_pics):
   return [p['large_url'] for p in all_pics if 'associated_rooms' in p and str(room['b_id']) in p['associated_rooms']] 

def parse_hotel(page):
    s = soup(page) 
    hotel_data_script = s.select_one("script[type='application/ld+json']")
    booking_data =  hotel_data_script.find_next_sibling('script').string
    room_data, pic_data = [re.search(rx, booking_data).group(1) for rx in [r"b_rooms_available_and_soldout: (\[[\s\S]+\]),",
                                                                            r"hotelPhotos: (\[[\s\S]+?\]),"]]
    hotel_data = hotel_data_script.string
    hotel, rooms, pics = [json5.loads(data) for data in [hotel_data, room_data, pic_data]]
    data = {}
    data['name'] = hotel['name']
    data['address'] = hotel['address']['streetAddress']
    data['rating'] = hotel['aggregateRating']['ratingValue']
    data['latlng'] = s.select_one("#hotel_sidebar_static_map").get("data-atlas-latlng")
    data['descr'] = hotel['description']
    data['url'] = hotel['url']
    data['img'] = hotel['image']
    data['pics'] = [p['large_url'] for p in pics if 'associated_rooms' not in p]
    data['rooms'] = dict([[r['b_name'], {'rates': room_pricing(r), 'pics': room_pics(r, pics)}] for r in rooms])
    return data

def index_urls(country, city, datein, dateout):
    url = "https://www.booking.com/searchresults.en-it.html?checkin_month={in_month}" \
        "&checkin_monthday={in_day}&checkin_year={in_year}&checkout_month={out_month}" \
        "&checkout_monthday={out_day}&checkout_year={out_year}&group_adults={people}" \
        "&group_children=0&order=price&ss={city}%2C%20{country}&selected_currency={currency}&shw_aparth={no_hostels}"\
        .format(in_month=str(datein.month),
                in_day=str(datein.day),
                in_year=str(datein.year),
                out_month=str(dateout.month),
                out_day=str(dateout.day),
                out_year=str(dateout.year),
                people=1,
                city=city,
                country=country,
                no_hostels=1,
                currency='EUR')
    return lambda n: url + "&offset=" + str(n * 25)

async def fetch(session, url):
    async with session.get(url, headers=HEADERS) as response:
        if response.status != 200:
            response.raise_for_status()
        return await response.text()

async def get_pages(session, urls):
    tasks = []
    for url in urls:
        task = asyncio.create_task(fetch(session, url))
        tasks.append(task)
    results = await asyncio.gather(*tasks)
    return results

async def scrape(country, city='', max_page=40):
    
    checkin = datetime.datetime.now() + datetime.timedelta(30)
    checkout = checkin + datetime.timedelta(1)

    urls = index_urls(country, city, checkin, checkout) 

    async with aiohttp.ClientSession() as session:

        if max_page > 1:
            print(f"[~] Fetching first page for page count")
            first_page = await fetch(session, urls(0)) 
            total_pages = int(page_count(soup(first_page)))
            print(f"[~] Total {total_pages} pages")
            max_page = min(max_page, total_pages)

        print(f"[~] fetching {max_page} pages")
        index_pages = await get_pages(session, [urls(n) for n in range(max_page)])

        print("[~] Extracting hotel links")
        
        links = []
        for page in index_pages:
           links.extend(hotel_links(soup(page)))

        links = set(links)

        print(f"[~] Found {len(links)} hotels, fetching...")

        hotel_pages = await (get_pages(session, links))

        print(f"[~] Parsing hotels")

        hotels = []
        for n, page in enumerate(hotel_pages):
            print('', end=f'\r[~] {n}')
            hotels.append(parse_hotel(page))

        print()

        outfile = f"./{country + city if city else country}0-{max_page}.json5"
        with open(outfile, 'w', encoding='utf-8') as f:
            json5.dump(hotels, f, ensure_ascii=False, indent=4)

        print(f"[~] Done, result in {outfile}")

def cmd_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--country",
                    help='filter by country',
                    default='')
    parser.add_argument("--city",
                    help='filter by city',
                    default='')
    parser.add_argument("-m", '--max-page',
                    type=int,
                    help='max number of pages to fetch')
    args = parser.parse_args()
    if args.country == '' and args.city == '':
        parser.error('Need --city or --country param')
    return vars(args)

if __name__ == "__main__":
    asyncio.run(scrape(country='Austria', max_page=1))
    #asyncio.run(scrape(**cmd_args()))

#test
#with open('hotel.html','r') as f:
#    parse_hotel(f.read())
